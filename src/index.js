import RequestService  from '@jclib/request-service';
import ResponseService from '@jclib/response-service-cli';

class ApiService {
  baseUrl;
  req;
  res = new ResponseService();
  urlKey;

  constructor(config) {
    this._init(config);
  }

  _buildQuery = (queryParams, queryString = '') => {
    if (typeof queryParams === 'string') {
      return `?${new URLSearchParams(`${queryString}${queryString.indexOf('?') >= 0 ? '&' : '?'}${queryParams}&`).toString()}`;
    }

    return `?${new URLSearchParams(Object.keys(queryParams).reduce((query, param) => `${query}${param}=${queryParams[param]}&`, '?')).toString()}`;
  };

  _getUrl = (url) => {
    const baseUrl = this.baseUrl.replace(/\/$/, '');
    const urlKey  = this.urlKey.replace(/^\//, '').replace(/\/$/, '');
    const isQuery = /^\?/.test(url);

    return `${baseUrl}/${urlKey}${isQuery ? url : `${urlKey.length > 0 ? '/' : ''}${url}`}`
      .replace(/&$/, '')
      .replace(/\?$/, '')
      .replace(/\/$/, '');
  };

  _init = (config) => {
    const {baseUrl, urlKey, ...axiosConfig} = config;

    this.baseUrl = baseUrl;
    this.urlKey  = urlKey;

    this.req = new RequestService(axiosConfig);
  };

  _processResponse = (promise, callback) => {
    const response = this.res.processResponse(promise)
      .then((res) => this.res.doSuccessAction(res))
      .catch((err) => this.res.doFailureAction(err));

    response.then(
      (res) => callback(false, res),
      (err) => callback(err),
    );
  };

  delete = (url, callback, options = {}) => {
    this._processResponse(this.req.delete(this._getUrl(url), options), callback);
  };

  get = (url, callback, options = {}) => {
    this._processResponse(this.req.get(this._getUrl(url), options), callback);
  };

  getUri = (callback, options = {}) => {
    callback(false, this.req.getUri(options));
  };

  head = (url, callback, options = {}) => {
    this._processResponse(this.req.head(this._getUrl(url), options), callback);
  };

  options = (url, callback, options = {}) => {
    this._processResponse(this.req.options(this._getUrl(url), options), callback);
  };

  patch = (url, data, callback, options = {}) => {
    this._processResponse(this.req.patch(this._getUrl(url), data, options), callback);
  };

  post = (url, data, callback, options = {}) => {
    this._processResponse(this.req.post(this._getUrl(url), data, options), callback);
  };

  put = (url, data, callback, options = {}) => {
    this._processResponse(this.req.put(this._getUrl(url), data, options), callback);
  };
}

export default ApiService;
export { RequestService, ResponseService };