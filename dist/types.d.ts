import RequestService                            from '@jclib/request-service';
import ResponseService, { ResponseServiceError } from '@jclib/response-service-cli';
import { AxiosPromise, AxiosRequestConfig }      from 'axios';

declare type ApiServiceConfig = AxiosRequestConfig & {baseUrl: string, formData?: boolean, urlKey?: string};
declare type ApiServiceCallback = (error: ResponseServiceError | boolean, response: {[p: string]: any}) => any;

declare interface ApiService {
}

declare class ApiService {
  protected baseUrl: string;
  protected req: RequestService;
  protected res: ResponseService;
  private urlKey: string;

  constructor(config: ApiServiceConfig);

  protected _buildQuery(queryParams: {[p: string]: number | string} | string, queryString?: string): string;

  protected _getUrl(url: string): string;

  private _init(config: ApiServiceConfig): void;

  protected _processResponse(promise: AxiosPromise, callback: ApiServiceCallback): Promise<any>

  protected delete(url: string, callback: ApiServiceCallback, options?: AxiosRequestConfig): Promise<any>;

  protected get(url: string, callback: ApiServiceCallback, options?: AxiosRequestConfig): Promise<any>;

  protected getUri(callback: ApiServiceCallback, options?: ApiServiceConfig): string;

  protected head(url: string, callback: ApiServiceCallback, options?: AxiosRequestConfig): Promise<any>;

  protected options(url: string, callback: ApiServiceCallback, options?: AxiosRequestConfig): Promise<any>;

  protected patch(url: string, data: {[p: string]: any}, callback: ApiServiceCallback, options?: AxiosRequestConfig): Promise<any>;

  protected post(url: string, data: {[p: string]: any}, callback: ApiServiceCallback, options?: AxiosRequestConfig): Promise<any>;

  protected put(url: string, data: {[p: string]: any}, callback: ApiServiceCallback, options?: AxiosRequestConfig): Promise<any>;
}

export default ApiService;
export { RequestService, ResponseService };