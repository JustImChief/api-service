"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "RequestService", {
  enumerable: true,
  get: function get() {
    return _requestService.default;
  }
});
Object.defineProperty(exports, "ResponseService", {
  enumerable: true,
  get: function get() {
    return _responseServiceCli.default;
  }
});
exports.default = void 0;

var _requestService = _interopRequireDefault(require("@jclib/request-service"));

var _responseServiceCli = _interopRequireDefault(require("@jclib/response-service-cli"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ApiService {
  constructor(_config) {
    var _this = this;

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "req", void 0);

    _defineProperty(this, "res", new _responseServiceCli.default());

    _defineProperty(this, "urlKey", void 0);

    _defineProperty(this, "_buildQuery", function (queryParams) {
      var queryString = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

      if (typeof queryParams === 'string') {
        return "?".concat(new URLSearchParams("".concat(queryString).concat(queryString.indexOf('?') >= 0 ? '&' : '?').concat(queryParams, "&")).toString());
      }

      return "?".concat(new URLSearchParams(Object.keys(queryParams).reduce((query, param) => "".concat(query).concat(param, "=").concat(queryParams[param], "&"), '?')).toString());
    });

    _defineProperty(this, "_getUrl", url => {
      var baseUrl = this.baseUrl.replace(/\/$/, '');
      var urlKey = this.urlKey.replace(/^\//, '').replace(/\/$/, '');
      var isQuery = /^\?/.test(url);
      return "".concat(baseUrl, "/").concat(urlKey).concat(isQuery ? url : "".concat(urlKey.length > 0 ? '/' : '').concat(url)).replace(/&$/, '').replace(/\?$/, '').replace(/\/$/, '');
    });

    _defineProperty(this, "_init", config => {
      var {
        baseUrl,
        urlKey
      } = config,
          axiosConfig = _objectWithoutProperties(config, ["baseUrl", "urlKey"]);

      this.baseUrl = baseUrl;
      this.urlKey = urlKey;
      this.req = new _requestService.default(axiosConfig);
    });

    _defineProperty(this, "_processResponse", (promise, callback) => {
      var response = this.res.processResponse(promise).then(res => this.res.doSuccessAction(res)).catch(err => this.res.doFailureAction(err));
      response.then(res => callback(false, res), err => callback(err));
    });

    _defineProperty(this, "delete", function (url, callback) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

      _this._processResponse(_this.req.delete(_this._getUrl(url), options), callback);
    });

    _defineProperty(this, "get", function (url, callback) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

      _this._processResponse(_this.req.get(_this._getUrl(url), options), callback);
    });

    _defineProperty(this, "getUri", function (callback) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      callback(false, _this.req.getUri(options));
    });

    _defineProperty(this, "head", function (url, callback) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

      _this._processResponse(_this.req.head(_this._getUrl(url), options), callback);
    });

    _defineProperty(this, "options", function (url, callback) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

      _this._processResponse(_this.req.options(_this._getUrl(url), options), callback);
    });

    _defineProperty(this, "patch", function (url, data, callback) {
      var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

      _this._processResponse(_this.req.patch(_this._getUrl(url), data, options), callback);
    });

    _defineProperty(this, "post", function (url, data, callback) {
      var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

      _this._processResponse(_this.req.post(_this._getUrl(url), data, options), callback);
    });

    _defineProperty(this, "put", function (url, data, callback) {
      var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

      _this._processResponse(_this.req.put(_this._getUrl(url), data, options), callback);
    });

    this._init(_config);
  }

}

var _default = ApiService;
exports.default = _default;